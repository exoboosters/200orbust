#!/bin/bash
# This shell script is a simple website monitor that sends a Slack notification when your Site goes down

# Change working directory to current
cd /root/200orbust

# Set common variables
INPLIST=urllists.txt
TMPLIST=tmplists.txt
UPDATED=false

# Remove temp file
rm -rf $TMPLIST

# Pull the latest URL list from git
git pull origin master

# This is a common function to post to slack
# It accepts three arguments 
# 1. The Actual message
# 2. The type of message (INFO/WARNING/ERROR)
# 3. An emoji for the message
function post_to_slack () {
  # format message as a code block ```${msg}```
  SLACK_MESSAGE="\`\`\`$1\`\`\`"
  SLACK_EMOJI=$3
  # Change this Variable to your slack hook
  SLACK_URL=https://hooks.slack.com/services/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

  case "$2" in
    INFO)
      SLACK_ICON=':heavy_check_mark:'
      ;;
    WARNING)
      SLACK_ICON=':warning:'
      ;;
    ERROR)
      SLACK_ICON=':x:'
      ;;
    *)
      SLACK_ICON=':slack:'
      ;;
  esac

  curl -X POST --data "payload={\"username\": \"200orbust\", \"icon_emoji\": \"${SLACK_EMOJI}\", \"text\": \"${SLACK_ICON} ${SLACK_MESSAGE}\"}" ${SLACK_URL}
}

# We can now loop through the contents of the file urllists.txt and check
# Format of the input URL\HTTP-STATUS-CODE
for line in $(cat $INPLIST); do

   # Split the input line into URL & last HTTP status code
   IFS='\';linArr=($line); unset IFS;
   url2Chk=${linArr[0]}
   httpcode=${linArr[1]}
   
   # We are just checking HTTP Status code. A more complex checker can parse the entire curl output and check for
   # known valid data
   status_code=$(curl -A "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0" --write-out %{http_code} --silent --output /dev/null -L $url2Chk)

   # Write URL and status_code into TMPLIST
   echo "${url2Chk}\\${status_code}" >> $TMPLIST

   if [ "$status_code" -ne "$httpcode" ] ; then
       # Status Code has changed. Check what has changed
       UPDATED=true
        if [ "$status_code" -ne 200 ] ; then
          emoji=":x:"
          msgTitle="$url2Chk is down"
          msgBody="$url2Chk appears down. Check returned a status code of $status_code at $(date)"
          post_to_slack "$msgBody" "ERROR" $emoji
        else
          emoji=":heavy_check_mark:"
          msgTitle="$url2Chk is up"
          msgBody="$url2Chk is back up. Checked at $(date)"
          post_to_slack "$msgBody" "INFO" $emoji
        fi
   fi

# End Do
done

# If the status has changed on any of the URLs, commit & push changes back to Repo
if [ "$UPDATED" = true ] ; then
   rm -rf $INPLIST
   mv $TMPLIST $INPLIST
   git add $INPLIST
   git commit -m "Status changed on some URLs"
   git push origin master
fi

exit 0
